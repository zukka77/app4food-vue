<?php
//a file that will output JSON data based on "magazines" database records
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
//
// NON FUNZIONA
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/homeless.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$homeless = new Homeless($db);

// set ID property of record to read
$homeless->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of product to be edited
$homeless->readOne();
  
  
// check if more than 0 record found
if($homeless->name!=null){
    // create array
        $homeless_arr=array(
            "id" => $homeless->id,
            "name" => $homeless->name,
            "note" => $homeless->note,
            "latitude" => $homeless->latitude,
            "longitude" => $homeless->longitude,
            "created" => $homeless->created,
            "modified" => $homeless->modified
        );
  
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show magazines data in json format
    echo json_encode($homeless_arr);
}
  
// no magazines found will be here
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No homeless found.")
    );
}

?>