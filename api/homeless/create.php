<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate homeless object
include_once '../objects/homeless.php';
  
$database = new Database();
$db = $database->getConnection();
  
$homeless = new Homeless($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if(
    !empty($data->name) &&
    !empty($data->latitude) &&
    !empty($data->longitude) &&
    !empty($data->need)&&
    !empty($data->place)&&
    !empty($data->note)

){
  
    // set homeless property values
    $homeless->name = $data->name;
    $homeless->latitude = $data->latitude;
    $homeless->longitude = $data->longitude;
    $homeless->need = $data->need;
    $homeless->place = $data->place;
    $homeless->note =$data->note;
    $homeless->type ="marker";
    //$homeless->created = date('Y-m-d H:i:s');
  
    // create the homeless
    if($homeless->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "homeless was created."));
    }
  
    // if unable to create the product, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        echo json_encode(array("message" => "Unable to create new item."));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create new item. Data is incomplete."));
}
?>