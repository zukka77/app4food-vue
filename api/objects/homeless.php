<?php
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
// contains properties and methods for "product" database queries.
class Homeless{
  
    // database connection and table name
    private $conn;
    private $table_name = "homeless";
  
    // object properties
    public $id;
    public $name;
    public $note;
    public $latitude;
    public $longitude;
    public $need;
    public $place;
    public $type;
    public $created;
    public $modified;
    //public $coords;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // used when filling up the update product form
    function readOne(){
 
        // query to read single record
        $query = "SELECT
                m.id, m.name, m.note, m.latitude, m.longitude
                FROM
                    " . $this->table_name . " m
                   
                WHERE
                    m.id = ?
                LIMIT
                    0,1";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
     
        // bind id of product to be updated
        $stmt->bindParam(1, $this->id);
     
        // execute query
        $stmt->execute();
     
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        // set values to object properties
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->note = $row['note'];
        $this->latitude = $row['latitude'];
        $this->longitude = $row['longitude'];
        $this->created = $row['created'];
        $this->modified = $row['modified'];
    }
    
    // read list of homelesses
    function read(){
        // select all query
        $query = "SELECT
                    m.id, m.name, m.note, m.latitude, m.longitude, m.created, m.modified
                FROM
                    " . $this->table_name . " m
                ORDER BY
                    m.id DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
        }
   // search products
    function search($keywords){
    
        // select all query
        $query = "SELECT
                    m.id, m.name, m.note, m.latitude, m.longitude, m.type, m.created, m.modified
                FROM
                    " . $this->table_name . " m
                    
                WHERE
                    m.name LIKE ? OR m.note LIKE ?
                ORDER BY
                    m.id ASC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
    
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
       // $stmt->bindParam(3, $keywords);
        // $stmt->bindParam(4, $keywords);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    } 
    // read list of homeless with pagination
    public function readPaging($from_record_num, $records_per_page){
    
        // select query
        $query = "SELECT
                m.id, m.name, m.note, m.latitude, m.longitude, m.created, m.modified
                FROM
                    " . $this->table_name . " m
                    
                ORDER BY m.id DESC
                LIMIT ?, ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();
    
        // return values from database
        return $stmt;
    }
    // used for paging products
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['total_rows'];
    }
    // create product
function create(){
  
    // query to insert record
    // name=:name, note=:note, latitude=:latitude, longitude=:longitude, need=:need, place=:place, type=:type";

    //$type = 'marker';
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                name=:name, note=:note, latitude=:latitude, longitude=:longitude, need=:need, place=:place, type=:type";
  
    // prepare query
    $stmt = $this->conn->prepare($query);
  
    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->note=htmlspecialchars(strip_tags($this->note));
    $this->latitude=htmlspecialchars(strip_tags($this->latitude));
    $this->longitude=htmlspecialchars(strip_tags($this->longitude));
    $this->need=htmlspecialchars(strip_tags($this->need));
    $this->place=htmlspecialchars(strip_tags($this->place));
    $this->type=htmlspecialchars(strip_tags($this->type));

    //$this->created=htmlspecialchars(strip_tags($this->created));
  
    // bind values
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":note", $this->note);
    $stmt->bindParam(":latitude", $this->latitude);
    $stmt->bindParam(":longitude", $this->longitude);
    $stmt->bindParam(":need", $this->need);
    $stmt->bindParam(":place", $this->place);
    $stmt->bindParam(":type", $this->type);
    //$stmt->bindParam(":created", $this->created);
  
    // execute query
    if($stmt->execute()){
        return true;
    }
  
    return false;
      
}
}
?>